import Analytics from "../components/Analytics";
/* eslint-disable @next/next/no-img-element */

// comps
import { SE0 } from "../components/SEO";
import { Header } from "../components/Header";
import { QuemSomos } from "../components/QuemSomos";
import { Servicos } from "../components/Servicos";
import { Contato } from "../components/Contato";
import { Footer } from "../components/Footer";
import { Clientes } from "../components/Clientes";
import { Depoimentos } from "../components/Depoimentos";

export default function Home() {
  return (
    <>
      <SE0
        title="Consultancy"
        description="Oferecemos soluções customizadas para seu modelo de trabalho através dos serviços que oferecemos alavancando seu negócio através de tecnologia."
      />
      <div className="wrapper">
        <Header />
        <QuemSomos identification="quem-somos" />
        <Servicos identification="servicos" />
        <Contato identification="contato" />
        <Clientes identification="clientes" />
        <Depoimentos identification="depoimentos" />
        <Footer />
      </div>
      <Analytics />
    </>
  );
}
