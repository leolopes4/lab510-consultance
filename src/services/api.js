import axios from 'axios';
import https from 'https';
var path = require( 'path' );

const api = axios.create({
    baseURL: `https://notification-api.lab510.com/api/v1`,
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
      })
})



export default api;
