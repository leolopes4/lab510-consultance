import styles from "./index.module.css";

export function Title({ text }: { text: string }) {
    return (
        <div className={styles.title}>
            <p>{text}</p>
        </div>
    )
}