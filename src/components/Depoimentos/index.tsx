/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @next/next/no-img-element */
import { Title } from "../../simpleComponents/Title";
import styles from "./index.module.css";

export function Depoimentos({ identification }: { identification: string }) {
  return (
    <>
      <section id={identification} >
        <Title text="#depoimentos" />
        <div className={styles.depoimentos}>
          <div className={styles.cards}>
            <div className={styles.card}>
            <div className={styles.rating}>
              <span className={styles.star}></span>
              <span className={styles.star}></span>
              <span className={styles.star}></span>
              <span className={styles.star}></span>
              <span className={styles.star}></span>
            </div>
              <div className={styles.textGroup}>
                <p>
                  "Estamos com um relacionamento ótimo com LAB510. A equipe está
                  sempre à disposição e os desenvolvedores bem qualificados, é o
                  início de uma grande parceria."
                </p><br />
                <h1>Donato Pina</h1>
                <h6>Owner BizCommerce</h6>
              </div>
            </div>
            <div className={styles.card}>
            <div className={styles.rating}>
              <span className={styles.star}></span>
              <span className={styles.star}></span>
              <span className={styles.star}></span>
              <span className={styles.star}></span>
              <span className={styles.star}></span>
            </div>
              <div className={styles.textGroup}>
                <p>
                  "Experiencia com LAB510 está sendo muito boa. Os profissionais
                  alocados estão performando e entregando resultado, atendendo a nossa
                  alta demanda."
                </p><br />
                
                <h1>Thiago Contardi</h1>
                <h6>Developer BizCommerce</h6>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
