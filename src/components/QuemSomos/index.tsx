import styles from "./index.module.css"

export function QuemSomos({ identification }: { identification: string }) {
  return (
    <section id={identification} >
      <div className={styles.quemSomos} >
        <div className={styles.quemSomosText}>
          <h1>Laboratório 510</h1>
          <p>
            Oferecemos soluções customizadas para seu modelo de trabalho
            através dos serviços que oferecemos alavancando seu negócio
            através de tecnologia.
          </p>
        </div>
      </div>
    </section>
  )
}