/* eslint-disable @next/next/no-img-element */
import styles from "./index.module.css"

export function Footer() {
  return (
    <footer className={styles.footer}>

        <a href="https://blog.lab510.com/">
          <img className={styles.logoHorizontal} src="/logo-horizontal.png" alt="" />
        </a>

        <div className={styles.socialDiv}>
          
          <a
            className={styles.socialList}
            href="https://www.facebook.com/lab510Company"
          >
            <img className={styles.social} src="/facebook.png" alt="" />
          </a>

          <a 
            className={styles.socialList} 
            href="https://www.instagram.com/lab_510/"
          >
            <img className={styles.social} src="/instagram.png" alt="" />
          </a>

          <a
            className={styles.socialList}
            href="https://www.linkedin.com/company/lab510/?originalSubdomain=br"
          >
            <img className={styles.social} src="/linkedin.png" alt="" />
          </a>

          <a href="https://www.youtube.com/channel/UCYGm2S-glUw56-j4ExwKE8Q?view_as=subscriber">
            <img className={styles.social} src="/youtube.png" alt="" />
          </a>

        </div>
        <div className={styles.line}></div>
        <span className={styles.copy}>
          LAB510 - © Copyright 2022 <br />
          47.727.324/0001-90
        </span>
      </footer>
  )
}