import Head from "next/head";
import { NextSeo } from "next-seo";

interface SEOProps {
  title: string;
  description?: string;
  image?: string;
}

export function SE0({ title, description, image }: SEOProps) {
  const pageTitle = title ? `LAB510 - ${title}` : undefined;
  const pageImage = image
    ? `${process.env.NEXT_PUBLIC_SITE_URL}${image}`
    : undefined;
  return (
    <Head>
      <title>{pageTitle}</title>
      {description && <meta name="description" content={description} />}
      {image && <meta name="image" content={pageImage} />}
      <meta name="robots" content="" />

      <meta httpEquiv="x-ua-compatible" content="IE=edge,chrome=1" />
      <meta name="MobileOptimized" content="320" />
      <meta name="HandheldFriendly" content="True" />
      <meta name="theme-color" content="#2397d4" />
      <meta name="msapplication-TileColor" content="#2397d4" />
      <meta name="referrer" content="no-referrer-when-downgrade" />
      <meta name="google" content="notranslate" />

      <meta property="og:title" content={pageTitle} />
      <meta property="og:description" content={description} />
      <meta property="og:locale" content="pt_BR" />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content={pageTitle} />
      <meta property="og:image" content={pageImage} />
      <meta property="og:image:secure_url" content={pageImage} />
      <meta property="og:image:alt" content="Thumbnail" />
      <meta property="og:image:type" content="image/png" />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630" />

      {/**TWITTER */}
      {/* <meta name="twitter:title" content={pageTitle} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:image" content={pageImage} />
      <meta name="twitter:image:src" content={pageImage} />
      <meta name="twitter:image:alt" content="Thumbnail" />
      <meta name="twitter:image:width" content="1200" />
      <meta name="twitter:image:height" content="620" /> */}
      <NextSeo
        title="LAB510 - Consultancy"
        description="Oferecemos soluções customizadas para seu modelo de trabalho através dos serviços que oferecemos alavancando seu negócio através de tecnologia."
        canonical="https://consultancy.lab510.com/"
      />
    </Head>
  );
}
