/* eslint-disable @next/next/no-img-element */
import { Title } from "../../simpleComponents/Title"
import styles from "./index.module.css"

export function Servicos({ identification }: { identification: string }) {
  return (
  <>
    <Title text="#serviços"/>
    <section id={identification}>
      <div className={styles.servicos}>
          <div className={styles.cards}>
            <img src="/dev.png" alt="Logo Lab510" />
            <div className={styles.cardText}>
              <h1>Desenvolvimento</h1>
              <br />
              <p>
                {" "}
                Se você procura desenvolvimento de sites, aplicativos e sistemas
                de alto nível, está no lugar certo!
              </p>
            </div>
          </div>
          <div className={styles.cards}>
            <img src="/manutencao.png" alt="Logo Lab510" />
            <div className={styles.cardText}>
              <h1>Manutenção</h1>
              <br />
              <p>
                Monitoramento e melhorias contínuas para manter tudo
                funcionando.
              </p>
            </div>
          </div>
          <div className={styles.cards}>
            <img src="/profi.png" alt="Logo Lab510" />
            <div className={styles.cardText}>
              <h1>Alocação de profissional</h1>
              <br />
              <p>
                {" "}
                Aqui você encontra profissionais especializados para qualquer
                trabalho!
              </p>
            </div>
          </div>
          <div className={styles.cards}>
            <img src="/equip.png" alt="Logo Lab510" />
            <div className={styles.cardText}>
              <h1>Alocação de equipes</h1>
              <br />
              <p>
                {" "}
                Já imaginou ter uma equipe completa e qualificada trabalhando no
                seu projeto?
              </p>
            </div>
          </div>
      </div>
    </section>
  </>
  )
}