/* eslint-disable @next/next/no-img-element */
import { Title } from "../../simpleComponents/Title";
import styles from "./index.module.css";

export function Clientes({ identification }: { identification: string }) {
  return (
    <>
      <Title text="#clientes" />
      <section id={identification} >
        <div className={styles.clientes}>
          <div className={styles.cards}>
            {/* <h1>Empresas que confiam na LAB510</h1> */}
              <img
                className={styles.logo}
                src="/optica-santista.png"
                alt="Logo Optica Santista"
              />
              <img
                className={styles.logo}
                src="/biz-commerce.png"
                alt="Logo BizCommerce"
              />
              <img
                className={styles.logo}
                src="/sa-imunne.png"
                alt="Logo s&a imunizações"
              />
          </div>
        </div>
      </section>
    </>
  );
}
