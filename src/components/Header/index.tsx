/* eslint-disable @next/next/no-img-element */
import { Link } from "react-scroll"
import styles from "./index.module.css"

export function Header() {
  return (
    <header className={styles.header}>
      <img className={styles.logo} src="/logo.png" alt="Logo Lab510" />
      <nav className={styles.nav}>
        <a href='https://blog.lab510.com' rel="noreferrer" target="_blank">
          Blog
        </a>
        <Link to='servicos' smooth={true} duration={500}>
          Serviços
        </Link>
        <Link to='contato' smooth={true} duration={500}>
          Contato
        </Link>
        <Link to='clientes' smooth={true} duration={500}>
          Clientes
        </Link>
        <Link to='depoimentos' smooth={true} duration={500}>
          Depoimentos
        </Link>
        <Link to='quem-somos' smooth={true} duration={500}>
          Quem Somos
        </Link>
      </nav>
    </header>
  )
}