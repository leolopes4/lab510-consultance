import { ChangeEvent, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import { AxiosResponse } from "axios";
import "react-toastify/dist/ReactToastify.css";
import styles from "./index.module.css"

// services
import api from '../../services/api'
import { Title } from "../../simpleComponents/Title";

/* eslint-disable @next/next/no-img-element */
export function Contato({ identification }: { identification: string }) {

  const [to, setTo] = useState("");
  const [subject, setSubject] = useState("");
  const [message, setMessage] = useState("");

  const handleChangeTo = (e: ChangeEvent<HTMLInputElement>) => {
    setTo(e.target.value);
  };

  const handleChangeMessage = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setMessage(e.target.value);
  };

  const handleChangeSubject = (e: ChangeEvent<HTMLInputElement>) => {
    setSubject(e.target.value);
  };

  const success_notify = (message: String) =>
    toast.success(`${message}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

  const fail_notify = (message: String) =>
    toast.error(`${message}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

  const clear_fields = () => {
    setTo("");
    setMessage("");
    setSubject("");
  };

  function doPost() {

    if (to === '' || subject === '' || message === '') {
      toast.error('Preencha todos os campos', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {

      const body = {
        to: "contato@lab510.com",
        subject: subject,
        message: `${to} - ${message}`,
      };
  
      api
        .post("/mail", body)
        .then((response: AxiosResponse) => {
          if (response.status == 200) {
            success_notify(response.data.message);
            clear_fields();
          }
        })
        .catch(() => {
          fail_notify("Falha ao enviar sua mensagem eletronica");
        });
    }    
  }
  
  return (
    <>
    <Title text="#fale-conosco"/>
    <section id={identification}>
      <div className={styles.contato}>
          <div className={styles.forms}>
          <img
              className={styles.contatoImg}
              src="bg-banner.png"
              alt="imagem de formulário"
            />
            <div className={styles.inputs}>
              <input
                onChange={handleChangeTo}
                value={to}
                type="email"
                placeholder="E-mail"
              />
              <input
                onChange={handleChangeSubject}
                value={subject}
                type="name"
                placeholder="Nome"
              />
              <textarea
                onChange={handleChangeMessage}
                value={message}
                placeholder="Como podemos te ajudar?"
              />
              <button onClick={doPost} type="submit" className={styles.button}>
                <img
                  className={styles.imgSubmit}
                  src="/enviar-mensagem.png"
                  alt="imagem de submit"
                />
              </button>
              <ToastContainer />
            </div>
          </div>
        </div>
        </section>
    </>
  )
}