FROM node:lts-bullseye

ENV NODE_OPTIONS=--openssl-legacy-provider

COPY . /tmp/src
WORKDIR /tmp/src

RUN yarn install;\ 
    yarn run build






# RUN git clone https://gitlab.com/leolopes4/blog-frontend.git ;\
#     cd blog-frontend ;\
#     git checkout feature/lab510-strategy;\
#     npm install;\
#     yarn run build

# WORKDIR /blog-frontend/

ENTRYPOINT ["yarn", "run", "start"]   
 
EXPOSE 4200

